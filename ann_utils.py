import json

import keras.backend as k
import tensorflow as tf
from math import pi
from keras.preprocessing.sequence import pad_sequences
from numpy import array, append


def pad_data(x, max_len, special_value):
    return pad_sequences(
        sequences=x,
        maxlen=max_len,
        dtype='float32',
        padding='post',
        truncating='post',
        value=special_value)


def split_data(x, y, split_ratio, batch_size):

    number_of_features_for_train = int(len(x) * split_ratio)
    number_of_features_for_train -= number_of_features_for_train % batch_size
    x_train = x[:number_of_features_for_train]
    y_train = y[:number_of_features_for_train]
    x_test = x[number_of_features_for_train:]
    y_test = y[number_of_features_for_train:]

    number_of_features_to_drop = len(x) % batch_size
    if number_of_features_to_drop > 0:
        x_test = x_test[: -1 * number_of_features_to_drop]
        y_test = y_test[: -1 * number_of_features_to_drop]

    return (x_train, y_train), (x_test, y_test)


def normalize(data):

    normalized_data = []
    for entry in data:
        new_entry = []
        for coordinate in entry:
            new_entry.append(coordinate[0])
            new_entry.append(coordinate[1])
        normalized_data.append(new_entry)
    return normalized_data


def reshape(x):
    for i in range(len(x)):
        first_reshape_size = int(len(x[i]) / 2)
        x[i] = array(x[i]).reshape((first_reshape_size, 2))
    return x


def remove_second_to_last_n_elements(x, number_of_elements_to_remove):
    new_x = x[:-number_of_elements_to_remove]
    append(new_x, x[-1])
    return new_x


def remove_excess_data(x_data, y_data):
    for i in range(len(x_data)):
        x_data_i_len = len(x_data[i])
        y_data_i_len = len(y_data[i])
        if x_data_i_len < y_data_i_len:
            number_of_elements_to_remove = y_data_i_len - x_data_i_len + 1
            y_data[i] = remove_second_to_last_n_elements(y_data[i], number_of_elements_to_remove)
        else:
            if x_data_i_len > y_data_i_len:
                number_of_elements_to_remove = x_data_i_len - y_data_i_len + 1
                x_data[i] = remove_second_to_last_n_elements(x_data[i], number_of_elements_to_remove)


def get_normalized_data(data_path, split_ratio, batch_size):

    x_data = []
    y_data = []
    with open(data_path) as json_file:
        data = json.load(json_file)
        for training_object in data:
            x_data.append(array(training_object['filedRoute']['coordinates']))
            y_data.append(array(training_object['actualRoute']['coordinates']))
    remove_excess_data(x_data, y_data)
    x_data = array(normalize(x_data))
    y_data = array(normalize(y_data))
    (x_train, y_train), (x_test, y_test) = split_data(x_data, y_data, split_ratio, batch_size)
    return (x_train, y_train), (x_test, y_test)


def get_data(data_path, split_ratio, batch_size, max_len, special_value):
    (x_train, y_train), (x_test, y_test) = get_normalized_data(data_path, split_ratio, batch_size)
    x_train = reshape(x_train)
    y_train = reshape(y_train)
    x_test = reshape(x_test)
    y_test = reshape(y_test)
    x_train = pad_data(x_train, max_len, special_value)
    y_train = pad_data(y_train, max_len, special_value)
    x_test = pad_data(x_test, max_len, special_value)
    y_test = pad_data(y_test, max_len, special_value)
    return (x_train, y_train), (x_test, y_test)


def orthodromic_distance_loss(actual_y, predicted_y):
    longitudes_radians_actual = actual_y[:, :, 0] * pi / 180
    longitudes_radians_predicted = predicted_y[:, :, 0] * pi / 180
    latitudes_radians_actual = actual_y[:, :, 1] * pi / 180
    latitudes_radians_predicted = predicted_y[:, :, 1] * pi / 180
    # get the difference between all expected and predicted longitudes
    d_longitudes = longitudes_radians_actual - longitudes_radians_predicted
    # get the difference between all expected and predicted latitudes
    d_latitudes = latitudes_radians_actual - latitudes_radians_predicted
    # using Haversine formula for orthodromic distance
    a = k.square(k.sin(d_latitudes / 2)) + k.cos(latitudes_radians_actual) * \
        k.cos(latitudes_radians_predicted) * k.square(k.sin(d_longitudes / 2))
    orthodromic_distance_c = 2 * tf.atan2(k.sqrt(a), k.sqrt(1 - a))
    return k.abs(orthodromic_distance_c)


def get_orthodromic_distance(point_a, point_b):
    longitude_a = point_a[0] * pi / 180
    longitude_b = point_b[0] * pi / 180
    latitude_a = point_a[1] * pi / 180
    latitude_b = point_b[1] * pi / 180
    delta_long = longitude_b - longitude_a
    delta_lat = latitude_b - latitude_a
    a = k.square(k.sin(delta_lat / 2)) + k.cos(latitude_a) * k.cos(latitude_b) * k.square(k.sin(delta_long / 2))
    dist = 2 * tf.atan2(k.sqrt(a), k.sqrt(1 - a))
    return k.abs(dist)


def punish_for_going_further_than_destination(actual_y, predicted_y):
    if actual_y.shape[0] is None:
        return [[0., 0.]]
    eps = 1e-5
    punishments = []
    for i in range(actual_y.shape[0]):
        current_punishments = []
        actual_coordinate_sequence = actual_y[i]
        predicted_coordinate_sequence = predicted_y[i]
        departure_airport_coordinates = actual_coordinate_sequence[0]
        arrival_airport_coordinates = actual_coordinate_sequence[-1]
        distance_between_airports = get_orthodromic_distance(departure_airport_coordinates, arrival_airport_coordinates)
        for current_coordinate in predicted_coordinate_sequence:
            distance = get_orthodromic_distance(departure_airport_coordinates, current_coordinate)
            delta_distance = k.eval(k.abs(distance - distance_between_airports))
            if delta_distance > eps:
                current_punishments.append([delta_distance * 10., delta_distance * 10.])
            else:
                current_punishments.append([0., 0.])
        punishments.append(current_punishments)
    return array(punishments)


def custom_mean_absolute_error(actual_y, predicted_y):
    mean_absolute_error = k.abs(actual_y - predicted_y)
    mean_absolute_error = mean_absolute_error + punish_for_going_further_than_destination(actual_y, predicted_y)
    return mean_absolute_error


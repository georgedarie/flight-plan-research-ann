import keras.backend as k
from keras.models import model_from_json, load_model
from math import pi
from numpy import array, float32

from ann_utils import normalize, orthodromic_distance_loss

json_file = open('flight-plan-prediction-model.json', 'r')
loaded_model_json = json_file.read()
json_file.close()
loaded_model = model_from_json(loaded_model_json)
loaded_model.load_weights("flight-plan-prediction-model.h5")

loaded_model = load_model('models/flight-plan-prediction-model-56-2.22675.hdf5', compile=False)
x = [
        [
            [19.89639, 60.12194],
            [18.30417, 58.69111],
            [16.390639, 57.578139],
            [14.836667, 56.13275],
            [13.4961056, 54.5059972],
            [11.8, 53.3483333],
            [10.5237635, 52.0],
            [9.249163917, 50.411946472],
            [9.221963889, 48.689877778]
        ]
    ]
actual_y = [
            [
                [19.89639, 60.12194],
                [4.775025, 52.293733333],
                [4.807725, 52.2141944],
                [5.27306, 52.12583],
                [7.6330556, 50.8147222],
                [8.4156141, 50.0434713],
                [8.7665278, 48.9739917],
                [8.9154448, 48.6857265],
                [9.221963889, 48.689877778]
            ]
        ]
normalized_x = array(normalize(x)).reshape((1, len(x[0]), 2))
normalized_actual_y = float32(array(normalize(actual_y)).reshape((1, len(actual_y[0]), 2)))
normalized_y = loaded_model.predict(normalized_x, verbose=1)
y = []
for coordinate in normalized_y[0]:
    y.append([coordinate[0], coordinate[1]])
print({
    "actual": {
        "type": "LineString",
        "coordinates": actual_y[0]
    },
    "predicted": {
        "type": "LineString",
        "coordinates": y
    }
})
print(
    'Orthodromic distance loss on normalized data:',
    k.eval(orthodromic_distance_loss(normalized_actual_y, normalized_y))
)

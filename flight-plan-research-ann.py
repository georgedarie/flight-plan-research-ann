from keras.callbacks import EarlyStopping, ModelCheckpoint, ReduceLROnPlateau
from keras.layers import LSTM, Dense, Masking
from keras.models import Sequential
from keras.optimizers import Adam, RMSprop
from matplotlib import pyplot

import ann_utils as utils

# declaring constants
split_ratio = 0.8
data_file_path = './training-data/training-data.json'
batch_size = 128
max_len = 42
special_value = -181
epochs = 200

# getting data
(x_train, y_train), (x_test, y_test) = utils.get_data(data_file_path, split_ratio, batch_size, max_len, special_value)

# building the model
early_stopping = EarlyStopping(
    patience=10,
    verbose=1,
    restore_best_weights=True,
    monitor='val_loss',
    mode='min'
)
save_best_only = ModelCheckpoint(
    filepath='models/flight-plan-prediction-model-{epoch:02d}-{val_loss:.5f}.hdf5',
    verbose=1,
    save_best_only=True,
    period=1,
    monitor='val_loss',
    mode='min'
)
reduce_lr_on_plateau = ReduceLROnPlateau(
    patience=15,
    verbose=1,
    factor=0.75,
    min_delta=1e-6,
    monitor='val_loss',
    mode='min'
)
callbacks = [save_best_only]
model = Sequential()
model.add(Masking(mask_value=special_value, input_shape=(None, 2)))
model.add(LSTM(units=1024, return_sequences=True, dropout=0.1))
model.add(Dense(2))
model.compile(
    optimizer=RMSprop(),
    loss=utils.custom_mean_absolute_error,
    metrics=[utils.orthodromic_distance_loss]
)
model.summary()

# training the model
history = model.fit(
    x_train,
    y_train,
    batch_size=batch_size,
    epochs=epochs,
    validation_data=[x_test, y_test],
    shuffle=False,
    callbacks=callbacks,
    verbose=2
)

# saving the model
with open("flight-plan-prediction-model.json", 'w') as json_model_file:
    json_model_file.write(model.to_json())
model.save_weights("flight-plan-prediction-model.h5")
print("Saved model to disk...")

# showing a graph of the mean_squared_error over time
pyplot.plot(history.history['val_loss'], 'b')
pyplot.plot(history.history['loss'], 'r')
pyplot.show()
